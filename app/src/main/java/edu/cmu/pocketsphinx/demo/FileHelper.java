package edu.cmu.pocketsphinx.demo;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class FileHelper {

    final static String NAME = "contacts";
    final static String CONTACTS_GRAMMAR_FILE = NAME + ".gram";
    final static String MD5_FILE = NAME + ".gram.md5";

    public static List<ContactData> createGrammarFile(Context context, List<ContactData> initialContactDataList) {
//        File grammarFile = new File(context.getFilesDir(), GRAMMAR_FILE);
        StringBuilder builder = new StringBuilder();

        List<String> contactNames = new ArrayList<>();
        for (ContactData data : initialContactDataList) {
            contactNames.add(data.getName());
        }

        List<String> dictionaryText = new ArrayList<>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open("cmudict-en-us.dict")));

            // do reading, usually loop until end of file reading
            String mLine;
            Log.d("FEO2", "1 Reading lines");
            int i = 0;
            while ((mLine = reader.readLine()) != null) {
                //process line
                Log.d("FEO2", "Line: " + mLine + "line : " + i++);
                String[] arr = mLine.split(" ");
                if (arr.length > 0) {
                    Log.d("FEO6", "Name: " + arr[0]);
                    dictionaryText.add(arr[0]);
                }
            }

        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }

        List<String> commonNames = new ArrayList<>(dictionaryText);
        commonNames.retainAll(contactNames);

        for (String commonName : commonNames) {
            Log.d("FEO7", "Common name: " + commonName);
        }

        builder.append("#JSGF V1.0;\n");
        builder.append("grammar contacts;\n");
        builder.append("<contact>=\n");

        for (int i = 0; i < commonNames.size(); i++) {
            String name = commonNames.get(i);
            if (i == commonNames.size() - 1) {
                builder.append(name + ";\n");
            } else {
                builder.append(name + "|\n");
            }
        }

        builder.append("public<contacts>=<contact>+;\n");

        String fileFinalContent = builder.toString();
        FileOutputStream outputStream;

        try {
            outputStream = context.openFileOutput(CONTACTS_GRAMMAR_FILE, Context.MODE_PRIVATE);
            outputStream.write(fileFinalContent.getBytes());
            outputStream.close();
        } catch (Exception e) {
            Log.d("FEO1", "ex1 : " + e.getMessage());
            e.printStackTrace();
        }

        FileOutputStream outputStream2;

        String finalContentMD5 = getMD5(fileFinalContent);
        try {
            outputStream2 = context.openFileOutput(MD5_FILE, Context.MODE_PRIVATE);
            outputStream2.write(finalContentMD5.getBytes());
            outputStream2.close();
        } catch (Exception e) {
            Log.d("FEO1", "ex2 : " + e.getMessage());
            e.printStackTrace();
        }

        List<ContactData> commonContacts = new ArrayList<>();
        for(ContactData initialContact : initialContactDataList){
            // find common
            if(commonNames.contains(initialContact.getName())){
                Log.d("FEO10", "adding : " + initialContact.getName());
                commonContacts.add(initialContact);
            }
        }

        Log.d("FEO10", "WORKS: " + commonNames.size() + ", " + commonContacts.size());

        return commonContacts;

    }

    public static File getContactsGrammarFile(Context context){
        return new File(context.getFilesDir(), CONTACTS_GRAMMAR_FILE);
    }

    public static String getMD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

}
