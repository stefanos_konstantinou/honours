package edu.cmu.pocketsphinx.demo;

/**
 * Created by Stefanos Konstantinou on 15/02/2018.
 */

public class ContactData {

    private String name;
    private String phoneNumber;

    public ContactData(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
