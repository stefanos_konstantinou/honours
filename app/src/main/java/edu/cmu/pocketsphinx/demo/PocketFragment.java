package edu.cmu.pocketsphinx.demo;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;


public class PocketFragment extends Fragment implements RecognitionListener {

    public static final String TAG = PocketFragment.class.getSimpleName();

    /* Named searches allow to quickly reconfigure the decoder */
    private static final String KWS_SEARCH = "wakeup";
    private static final String CONTACT_SEARCH = "contacts";

    /* Keyword we are looking for to activate menu */
    private static final String KEYPHRASE = "start emergency";

    private SpeechRecognizer recognizer;

    private TextView tvInfo;
    private Button btnSetupRecognizer;
    private TextView tvRecognizerOutput;

    private SetupListener mListener;

    private String recognizerPreparedHeadline;

    private List<ContactData> contactData;


    public List<ContactData> getContactData() {
        return contactData;
    }

    public void setContactData(List<ContactData> contactData) {
        this.contactData = contactData;
    }

    public interface SetupListener {
        void onSetupCompleted();
    }


    public void setupRecognizer() {

    }

    public PocketFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SetupListener) {
            mListener = (SetupListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (recognizer != null) {
            recognizer.cancel();
            recognizer.shutdown();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pocket, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // setup
        tvInfo = view.findViewById(R.id.tvInfo);

        btnSetupRecognizer = view.findViewById(R.id.btnSetupRecognizer);
        btnSetupRecognizer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupOrganizer();
            }
        });

        tvRecognizerOutput = view.findViewById(R.id.tvRecognizerOutput);

        recognizerPreparedHeadline = "Recognizer set up successful\n" + String.format(getResources().getString(R.string.kws_caption), KEYPHRASE);

    }
    // Recognizer initialization is a time-consuming and it involves IO,
    // so we execute it in async task
    private void setupOrganizer() {
        new SetupTask(this).execute();
    }



    private class SetupTask extends AsyncTask<Void, Void, Boolean> {

        private PocketFragment holder;

        public SetupTask(PocketFragment holder) {
            this.holder = holder;
        }

        public void setHolder(PocketFragment holder) {
            this.holder = holder;
        }

        @Override
        protected void onPreExecute() {
            tvInfo.setText("Preparing the recognizer");
            btnSetupRecognizer.setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                List<ContactData> commonContacts = FileHelper.createGrammarFile(holder.getActivity(), getContactList());
                holder.setContactData(commonContacts);

                Assets assets = new Assets(holder.getActivity());
                File assetsDir = assets.syncAssets();

                // The recognizer can be configured to perform multiple searches
                // of different kind and switch between them
                recognizer = SpeechRecognizerSetup.defaultSetup()
                        .setAcousticModel(new File(assetsDir, "en-us-ptm"))
                        .setDictionary(new File(assetsDir, "cmudict-en-us.dict"))
//                .setRawLogDir(assetsDir) // To disable logging of raw audio comment out this call (takes a lot of space on the device)
                        .getRecognizer();

                recognizer.addListener(holder);

                // Create keyword-activation search.
                recognizer.addKeyphraseSearch(KWS_SEARCH, KEYPHRASE);

                File contactsFile = FileHelper.getContactsGrammarFile(holder.getActivity());
                recognizer.addGrammarSearch(CONTACT_SEARCH, contactsFile);


            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if(success) {
                tvInfo.setText(recognizerPreparedHeadline);
                holder.switchSearch(KWS_SEARCH);
            } else {
                recognizer = null;
                holder.setContactData(null);
            }
        }

        private List<ContactData> getContactList() {
            List<ContactData> contactsData = new ArrayList<>();
            ContentResolver cr = holder.getActivity().getContentResolver();
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                    null, null, null, null);

            if ((cur != null ? cur.getCount() : 0) > 0) {
                while (cur != null && cur.moveToNext()) {
                    String id = cur.getString(
                            cur.getColumnIndex(ContactsContract.Contacts._ID));

                    String name = cur.getString(cur.getColumnIndex(
                            ContactsContract.Contacts.DISPLAY_NAME));

                    String arr[] = name.split(" ", 2);

                    String firstName = arr[0];   //the
                    Pattern p = Pattern.compile("^[a-zA-Z]+$");
                    Matcher m = p.matcher(firstName);
                    boolean onlyNormalLetters = m.find();
                    Log.i("FEO3", "NAAAME: " + name);
                    if (firstName != null && !firstName.isEmpty() && onlyNormalLetters) {
                        if (cur.getInt(cur.getColumnIndex(
                                ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                            Cursor pCur = cr.query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                    new String[]{id}, null);
                            while (pCur.moveToNext()) {
                                String phoneNo = pCur.getString(pCur.getColumnIndex(
                                        ContactsContract.CommonDataKinds.Phone.NUMBER));
                                Log.i("FEO3", "Name: " + name);
                                Log.i("FEO3", "Phone Number: " + phoneNo);
                                if(phoneNo != null && !phoneNo.isEmpty()){
                                    contactsData.add(new ContactData(name.toLowerCase(), phoneNo));
                                }
                            }
                            pCur.close();
                        }
                    }

                }
            }
            if (cur != null) {
                cur.close();
            }

            for(ContactData data : contactsData){
                Log.d("FEO5", "Name: " + data.getName() + ": " + data.getPhoneNumber());
            }

            return contactsData;
        }

    }

    @Override
    public void onBeginningOfSpeech() {
        tvRecognizerOutput.setText("");
    }


    /**
     * We stop recognizer here to get a final result
     * In partial result we get quick updates about current hypothesis. In
     * keyword spotting mode we can react here, in other modes we need to wait
     * for final result in onResult.
     */
    @Override
    public void onEndOfSpeech() {
        if (!recognizer.getSearchName().equals(KWS_SEARCH))
            switchSearch(KWS_SEARCH);
    }

    /**
     * THIS IS WHERE EVERYTHING HAPPENS
     * @param hypothesis
     */
    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if(hypothesis == null) return;

        String text = hypothesis.getHypstr();
        if (text.equals(KEYPHRASE))
            switchSearch(CONTACT_SEARCH);
        else {
            tvRecognizerOutput.append(text + ", ");
        }

    }
    /**
     * This callback is called when we stop the recognizer.
     */
    @Override
    public void onResult(Hypothesis hypothesis) {
        if(hypothesis != null) {
            tvInfo.setText("On Result : " + hypothesis.getHypstr());
            Toast.makeText(getActivity(), hypothesis.getHypstr(), Toast.LENGTH_SHORT).show();
            callContact(hypothesis.getHypstr());
        }
    }

    public void callContact(String query){
        ContactData contactToCall = null;
        for(ContactData c : contactData){
            if(c.getName().contains(query)){
                contactToCall = c;
                break;
            }
        }
        if(contactToCall != null){
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + contactToCall.getPhoneNumber()));
            startActivity(intent);
        }
    }


    @Override
    public void onError(Exception e) {
        tvInfo.setText(e.getMessage());
    }

    @Override
    public void onTimeout() {
        switchSearch(KWS_SEARCH);
        tvInfo.setText(recognizerPreparedHeadline);
    }

    private void switchSearch(String searchName) {
        recognizer.stop();

        // If we are not spotting, start listening with timeout (10000 ms or 10 seconds).
        if (searchName.equals(KWS_SEARCH))
            recognizer.startListening(searchName);
        else
            recognizer.startListening(searchName, 10000);

        tvInfo.setText(recognizerPreparedHeadline);
    }

}
